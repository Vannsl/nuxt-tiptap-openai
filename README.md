# Tiptap Open AI Demo

👩‍💻 Built with [Nuxt.js](https://nuxt.com/) and [TailwindCSS](https://tailwindcss.com/).


## Setup and Usage

For the Setup and Usage, check out the [Readme of the `/app`](https://gitlab.com/Vannsl/nuxt-tiptap-openai/-/blob/main/app/README.md?ref_type=heads).

## Connect with Open AI

To connect this project with your Open AI key:

1. Make a copy of `/app/.env.example` and call it `/app/.env`
1. Add your Open AI key to the env file
1. Enable the fetch request in `/app/server/api/ai.post.ts`

## Demo

![Demo Video](demo.mov)

## Talk Recordings

- [Vue.js Amsterdam](https://www.youtube.com/watch?v=GqNWZVwjNT8)
- [Frontend Nation](https://www.youtube.com/watch?v=QD3NVP2SZW4)